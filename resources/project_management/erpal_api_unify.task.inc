<?php

function erpal_api_unify_task_resource_definition() {
  $task = array(
    'task' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a Task',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.task'),
          'callback' => 'erpal_api_unify_task_resource_retrieve',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the task to retrieve',
            ),
          ),
          'access callback' => 'erpal_api_unify_task_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a Task',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.task'),
          'callback' => 'erpal_api_unify_task_resource_create',
          'args' => array(
            array(
              'name' => 'name',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The name of task',
              'type' => 'array',
            ),
          ),
          'access callback' => 'erpal_api_unify_task_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a Task',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.task'),
          'callback' => 'erpal_api_unify_task_resource_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the task to update',
            ),
            array(
              'name' => 'name',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The name of task',
              'type' => 'array',
            ),
          ),
          'access callback' => 'erpal_api_unify_task_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a Task'),
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.task'),
          'callback' => 'erpal_api_unify_task_resource_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the Task to delete',
            ),
          ),
          'access callback' => 'erpal_api_unify_task_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all tasks',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.task'),
          'callback' => 'erpal_api_unify_task_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'items_per_page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'items_per_page'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
      ),
      'relationships' => array(
        'time-entries' => array(
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.task'),
          'help'   => 'This method returns time-entries associated with a task.',
          'access arguments' => array('access content'),
          'callback' => 'erpal_api_unify_time_resource_time_entries',
          'args'     => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the time whose time-entries we are getting',
            ),
            array(
              'name' => 'items_per_page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'items_per_page'),
            ),
          ),
        ),
      ),
    ),
  );
  return $task;
}

/**
 * Returns the results of a node_load() for the specified task.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the task we want to return.
 * @return
 *   Task object or FALSE if not found.
 *
 * @see node_load()
 */
function erpal_api_unify_task_resource_retrieve($nid) {
  $node = node_load($nid);

  if ($node && $node->type == 'erpal_task') {
    $task = new stdClass();
    $task->name = $node->title;
    $task->author_id = $node->uid;
    $body = field_get_items('node', $node, 'body');
    if (!empty($body[0]['value'])) {
      $task->description = $body[0]['value'];
    }
    $parent = field_get_items('node', $node, 'field_task_parent');
    if (!empty($parent[0]['target_id'])) {
      $task->parent_task_id = $parent[0]['target_id'];
    }
    return $task;
  }
  else {
    return services_error(t('Task not found'), 404);
  }
}

/**
 * Creates a new Task based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $task
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   An associative array contained the new node's nid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_form_submit()
 */
function erpal_api_unify_task_resource_create($task) {
  $task = erpal_api_unify_task_prepare_data($task);
  try {
    $node = entity_create('node', $task);
    node_save($node);
    return array('id' => $node->nid);
  }
  catch (Exception $exception) {
    return services_error($exception->getMessage(), 406);
  }
}


/**
 * Updates a new task based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $nid
 *   Node ID of the task we're editing.
 * @param $task
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   The node's nid.
 *
 * @see drupal_form_submit()
 */
function erpal_api_unify_task_resource_update($nid, $task) {
  $node_task = node_load($nid);
  $old_node_task = get_object_vars($node_task);
  $task = erpal_api_unify_task_prepare_data($task);
  $task = array_merge($old_node_task, $task);

  try {
    node_save((object) $task);
    return array('id' => $nid);
  }
  catch (Exception $exception) {
    return services_error($exception->getMessage(), 406);
  }
}

/**
 * Prepare data for node resource definition.
 */
function erpal_api_unify_task_prepare_data($task) {
  module_load_include('inc', 'erpal_api_unify', 'resources/project_management/erpal_api_unify.project');

  // Use prepare function from project resource.
  $node = erpal_api_unify_project_prepare_data($task, 'erpal_task');
  
  // Prepare task properties for currect working with node API.
  $prepare = function (&$node, $task) {

    // Set project to the task.
    if (!empty($task['project_id']) && is_numeric($task['project_id'])) {
      if (empty($node['field_task_project'])) {
        $node['field_task_project'][$node['language']][0]['target_id'] = $task['project_id'];
      }
    }

    // Set parent task to the task.
    if (!empty($task['parent_task_id']) && is_numeric($task['parent_task_id'])) {
      if (empty($node['field_task_parent'])) {
        $node['field_task_parent'][$node['language']][0]['target_id'] = $task['parent_task_id'];
      }
    }
  };

  if (!empty($task[0]) && is_array($task[0])) {
    foreach ($task as $key => &$task_instance) {
      if (is_array($task_instance)) {
        $prepare($node[$key], $task_instance);
        $task_instance = $node[$key];
      }
    }
  }
  else {
    $prepare($node, $task);
    $task = $node;
  }

  // Use prepare function from project resource.
  return $task;
}

/**
 * Delete a task given its nid.
 *
 * @param int $nid
 *   Node ID of the task we're deleting.
 * @return bool
 *   Always returns true.
 */
function erpal_api_unify_task_resource_delete($nid) {
  node_delete($nid);
  return TRUE;
}

/**
 * Return an array of optionally paged nids baed on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/node?fields=nid,vid&parameters[nid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only nid and vid defined, where
 * nid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @return
 *   An array of node objects.
 *
 * @todo
 *   Evaluate the functionality here in general. Particularly around
 *     - Do we need fields at all? Should this just return full nodes?
 *     - Is there an easier syntax we can define which can make the urls
 *       for index requests more straightforward?
 */
function erpal_api_unify_task_resource_index($page, $fields, $parameters, $page_size) {
  module_load_include('inc', 'erpal_api_unify', 'resources/project_management/erpal_api_unify.project');
  $parameters['type'] = 'erpal_task';
  return erpal_api_unify_node_resource_index($page, $fields, $parameters, $page_size);
}

/**
 * Determine whether the current user can access a task resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see node_access()
 */
function erpal_api_unify_task_resource_access($op = 'view', $args = array()) {
  foreach ($args as $arg) {
    // Ensure that we have id of task node.
    if (is_numeric($arg)) {
      $task = node_load($arg);
      if ($task->type != 'erpal_task') {
        return services_error(t('Task not found'), 404);
      }
    }

    // Check referenced fields.
    $fields = array(

       // Check if we have referenced project.
      'project_id' => array(
        'function' => 'node_load',
        'name' => t('Project'),
      ),

      // Check if we have referenced user.
      'user_id' => array(
        'function' => 'user_load',
        'name' => t('User'),
      ),
    );

    if ($errors = erpal_api_unify_fields_validate($arg, $fields)) {
      return $errors;
    }

    // Name for task node required.
    if (in_array($op, array('create', 'update')) && is_array($arg) && empty($arg['name'])) {
      return services_error(t('Task name is required'), 406);
    }
  }
  return _node_resource_access($op, erpal_api_unify_task_prepare_data($args));
}

/**
 * Generates an array time entries attached to a time.
 *
 * @param $nid
 *   Number. Node ID
 * @return
 *   Array. A list of all time entries from the given node
 */
function erpal_api_unify_time_resource_time_entries($nid, $page_size) {
    module_load_include('inc', 'erpal_api_unify', 'resources/project_management/erpal_api_unify.time_entry');
  $query = new EntityFieldQuery();

  // Get all time entries.
  $query->entityCondition('entity_type', 'erpal_output')
    ->entityCondition('bundle', 'time')
    ->fieldCondition('field_output_time_refers', 'target_id', $nid);
  $result = $query->execute();

  if (!empty($result['erpal_output'])) {
    foreach ($result['erpal_output'] as $task) {
      $items[] = $task->output_id;
    }
  }
  return erpal_api_unify_time_entry_resource_index(0, '*', array('output_id' => implode(',', $items)), $page_size);
}
