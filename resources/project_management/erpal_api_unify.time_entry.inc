<?php

function erpal_api_unify_time_entry_resource_definition() {
  $time_entry = array(
    'time_entry' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a Time entry',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.time_entry'),
          'callback' => 'erpal_api_unify_time_entry_resource_retrieve',
          'args' => array(
            array(
              'name' => 'id',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The id of the time entry to retrieve',
            ),
          ),
          'access callback' => 'erpal_api_unify_time_entry_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a Time entry',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.time_entry'),
          'callback' => 'erpal_api_unify_time_entry_resource_create',
          'args' => array(
            array(
              'name' => 'subject',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The subject of time entry',
              'type' => 'array',
            ),
          ),
          'access callback' => 'erpal_api_unify_time_entry_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a Time entry',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.time_entry'),
          'callback' => 'erpal_api_unify_time_entry_resource_update',
          'args' => array(
            array(
              'name' => 'id',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The id of the time entry to update',
            ),
            array(
              'name' => 'name',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The name of time entry',
              'type' => 'array',
            ),
          ),
          'access callback' => 'erpal_api_unify_time_entry_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a time entry'),
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.time_entry'),
          'callback' => 'erpal_api_unify_time_entry_resource_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the time entry to delete',
            ),
          ),
          'access callback' => 'erpal_api_unify_time_entry_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all time entries',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.time_entry'),
          'callback' => 'erpal_api_unify_time_entry_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'items_per_page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'items_per_page'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
      ),
    ),
  );
  return $time_entry;
}

/**
 * Returns the results of a erpal_output_load() for the specified time entry.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $id
 *   ID of the time entry we want to return.
 * @return
 *   Time entry object or FALSE if not found.
 *
 * @see node_load()
 */
function erpal_api_unify_time_entry_resource_retrieve($id) {
  $erpal_output = erpal_output_load($id);

  if ($erpal_output && $erpal_output->type == 'time') {
    $time_entry = new stdClass();
    $time_entry->id = $erpal_output->output_id;
    $time_entry->subject = $erpal_output->title;
    $time_entry->author_id = $erpal_output->uid;

    // Prepare field duration.
    $time = field_get_items('erpal_output', $erpal_output, 'field_output_unit');
    $unit = units_unit_load($time[0]['target_id']);
    $hours = units_convert($time[0]['value'], $unit->machine_name, 'hour');
    $time_entry->duration = 60 * 60 * $hours;
    return $time_entry;
  }
  else {
    return services_error(t('Time entry not found'), 404);
  }
}

/**
 * Creates a new time entry based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $time_entry
 *   Array representing the attributes a erpal_output edit form would submit.
 * @return
 *   An associative array contained the new node's nid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_form_submit()
 */
function erpal_api_unify_time_entry_resource_create($time_entry) {
  $time_entry = erpal_api_unify_time_entry_prepare_data($time_entry);
  try {
    $output = entity_create('erpal_output', $time_entry);
    erpal_output_save($output);
    return array('id' => $output->output_id);
  }
  catch (Exception $exception) {
    return services_error($exception->getMessage(), 406);
  }
}

/**
 * Prepare data for time entry resource definition.
 */
function erpal_api_unify_time_entry_prepare_data($time_entry) {
  module_load_include('inc', 'erpal_output', 'includes/erpal_output_resource');
  $erpal_output = array();

  // Prepare time entry properties for currect working with entity API.
  $prepare = function (&$erpal_output, $time_entry) {

    // If language is passed use it, if not use language none.
    if (!empty($time_entry['language'])) {
      $erpal_output['language'] = $time_entry['language'];
    }
    else {
      $erpal_output['language'] = LANGUAGE_NONE;
    }

    // Set time entry subject to the node title.
    if (!empty($time_entry['subject'])) {
      if (empty($erpal_output['title'])) {
        $erpal_output['title'] = $time_entry['subject'];
      }
    }

    // Set time entry time to the node body.
    if (!empty($time_entry['duration'])) {
      if (empty($erpal_output['field_output_unit'])) {
        $unit = units_unit_machine_name_load('hour');
        $hours = $time_entry['duration'] / 60 / 60;
        $erpal_output['field_output_unit'][$erpal_output['language']][0] = array(
          'value' => round($hours, 2),
          'target_id' => $unit->umid,
        );
      }
    }

    // Set project/task to the time entry.
    if (!empty($time_entry['task_id'])) {
      if (empty($erpal_output['field_output_time_refers'])) {
        $erpal_output['field_output_time_refers'][$erpal_output['language']][0]['target_id'] = $time_entry['task_id'];
      }
    }

    // Set project/task to the time entry.
    if (!empty($time_entry['project_id'])) {
      if (empty($erpal_output['field_output_time_refers'])) {
        $erpal_output['field_output_time_refers'][$erpal_output['language']][0]['target_id'] = $time_entry['project_id'];
      }
    }

    // Set flag billable to the time entry.
    if (isset($time_entry['billable'])) {
      $erpal_output['field_output_billable'][$erpal_output['language']][0]['value'] = (int) $time_entry['billable'];
    }

    // Set author to the time entry.
    $erpal_output['uid'] = 1;
    if (!empty($time_entry['user_id']) && is_numeric($time_entry['user_id'])) {
      if (user_load($time_entry['user_id'])) {
        $erpal_output['uid'] = $time_entry['user_id'];
      }
    }
    // Set author to the time entry.
    if (!empty($time_entry['author_id']) && is_numeric($time_entry['author_id'])) {
      if (user_load($time_entry['author_id'])) {
        $erpal_output['uid'] = $time_entry['author_id'];
      }
    }

    // Project is "project" node bundle.
    if (!empty($time_entry['type'])) {
      $erpal_output['type'] = $time_entry['type'];
    }
    else {
      $erpal_output['type'] = 'time';
    }

  };

  if (!empty($time_entry[0]) && is_array($time_entry[0])) {
    foreach ($time_entry as &$time_entry_instance) {
      $time_entry($erpal_output, $time_entry_instance);
      $time_entry_instance = $erpal_output;
    }
  }
  else {
    $prepare($erpal_output, $time_entry);
    $time_entry = $erpal_output;
  }
  return $time_entry;
}

/**
 * Updates a new time entry based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $id
 *   time_entry ID of the time entry we're editing.
 * @param $time_entry
 *   Array representing the attributes a time entry edit form would submit.
 * @return
 *   The time_entry's id.
 *
 * @see drupal_form_submit()
 */
function erpal_api_unify_time_entry_resource_update($id, $time_entry) {
  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'erpal_output_ui', 'erpal_output_ui.pages');
  $time_entry['id'] = $time_entry['output_id'] = $id;
  $time_entry = erpal_api_unify_time_entry_prepare_data($time_entry);
  $old_output = erpal_output_load($id);
  $old_time_entry = get_object_vars($old_output);
  $time_entry = array_merge($old_time_entry, $time_entry);
  try {
    erpal_output_save((object) $time_entry);
    return array('id' => $id);;
  }
  catch (Exception $exception) {
    return services_error($exception->getMessage(), 406);
  }
}

/**
 * Delete a time entry given its nid.
 *
 * @param int $id
 *   Time entry ID of the time entry we're deleting.
 * @return bool
 *   Always returns true.
 */
function erpal_api_unify_time_entry_resource_delete($id) {
  erpal_output_delete_multiple(array($id));
  return TRUE;
}

/**
 * Return an array of optionally paged nids baed on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/node?fields=nid,vid&parameters[nid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only nid and vid defined, where
 * nid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @return
 *   An array of node objects.
 *
 * @todo
 *   Evaluate the functionality here in general. Particularly around
 *     - Do we need fields at all? Should this just return full nodes?
 *     - Is there an easier syntax we can define which can make the urls
 *       for index requests more straightforward?
 */
function erpal_api_unify_time_entry_resource_index($page, $fields, $parameters, $page_size) {
  erpal_api_unify_set_pager();
  $parameters['type'] = 'time';
  $aids = db_select('erpal_output', 't')
    ->extend('PagerDefault')
    ->limit($page_size)
    ->orderBy('created', 'DESC');
  erpal_api_unify_resource_build_index_query($aids, $page, $fields, $parameters, $page_size, 'erpal_output');
  $outputs = services_resource_execute_index_query($aids);

  $time_entries = erpal_api_unify_list_settings($page_size);
  foreach ($outputs as $output) {
    $time_entries['items'][] = erpal_api_unify_time_entry_resource_retrieve($output->output_id);
  }
  return $time_entries;
}

/**
 * Determine whether the current user can access a time entry resource.
 *
 * @param $op
 *   One of view, update, create, delete per erpal_output_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see erpal_output_access()
 */
function erpal_api_unify_time_entry_resource_access($op = 'view', $args = array()) {
  module_load_include('inc', 'erpal_output', 'includes/erpal_output_resource');
  foreach ($args as &$arg) {

    // Ensure that we have id of time entry.
    if (is_numeric($arg)) {
      $time_entry = erpal_output_load($arg);
      if ($time_entry && $time_entry->type != 'time') {
        return services_error(t('Time entry not found'), 404);
      }
    }

    // Check referenced fields.
    $fields = array(

       // Check if we have referenced project.
      'project_id' => array(
        'function' => 'node_load',
        'name' => t('Project'),
      ),

      // Check if we have referenced task.
      'task_id' => array(
        'function' => 'node_load',
        'name' => t('Task'),
      ),

      // Check if we have referenced user.
      'user_id' => array(
        'function' => 'user_load',
        'name' => t('User'),
      ),
    );

    if ($errors = erpal_api_unify_fields_validate($arg, $fields)) {
      return $errors;
    }

    // Subject/duration for time entry required.
    if (in_array($op, array('create', 'update')) && is_array($arg)) {
      if (empty($arg['subject'])) {
        return services_error(t('Time entry Subject is required'), 406);
      }
      if (empty($arg['duration'])) {
        return services_error(t('Time entry duration is required'), 406);
      }
      else {
        if (!is_numeric($arg['duration'])) {
          return services_error(t('Time entry duration should be numeric'), 406);
        }
      }
      $arg['type'] = 'time';
    }
  }
  return _erpal_output_resource_access($op, $args);
}
