<?php

function erpal_api_unify_project_resource_definition() {
  $project = array(
    'project' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a Project',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'callback' => 'erpal_api_unify_project_resource_retrieve',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the project to retrieve',
            ),
          ),
          'access callback' => 'erpal_api_unify_project_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a Project',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'callback' => 'erpal_api_unify_project_resource_create',
          'args' => array(
            array(
              'name' => 'name',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The name of project',
              'type' => 'array',
            ),
          ),
          'access callback' => 'erpal_api_unify_project_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a Project',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'callback' => 'erpal_api_unify_project_resource_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the project to update',
            ),
            array(
              'name' => 'name',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The name of project',
              'type' => 'array',
            ),
          ),
          'access callback' => 'erpal_api_unify_project_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a Project'),
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'callback' => 'erpal_api_unify_project_resource_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the Project to delete',
            ),
          ),
          'access callback' => 'erpal_api_unify_project_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all projects',
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'callback' => 'erpal_api_unify_project_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'items_per_page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'items_per_page'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
      ),
      'relationships' => array(
        'tasks' => array(
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'help'   => 'This method returns tasks associated with a project.',
          'access arguments' => array('access content'),
          'callback' => 'erpal_api_unify_project_resource_tasks',
          'args'     => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the project whose tasks we are getting',
            ),
            array(
              'name' => 'items_per_page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'items_per_page'),
            ),
          ),
        ),
        'time-entries' => array(
          'file' => array('type' => 'inc', 'module' => 'erpal_api_unify', 'name' => 'resources/project_management/erpal_api_unify.project'),
          'help'   => 'This method returns time-entries associated with a project.',
          'access arguments' => array('access content'),
          'callback' => 'erpal_api_unify_project_resource_time_entries',
          'args'     => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the project whose time-entries we are getting',
            ),
            array(
              'name' => 'items_per_page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'items_per_page'),
            ),
          ),
        ),
      ),
    ),
  );
  return $project;
}

/**
 * Returns the results of a node_load() for the specified project.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the project we want to return.
 * @return
 *   Project object or FALSE if not found.
 *
 * @see node_load()
 */
function erpal_api_unify_project_resource_retrieve($nid) {
  $node = node_load($nid);

  if ($node && $node->type == 'erpal_project') {
    $project = new stdClass();
    $project->name = $node->title;
    $project->author_id = $node->uid;
    $body = field_get_items('node', $node, 'body');
    if (!empty($body[0]['value'])) {
      $project->description = $body[0]['value'];
    }
    return $project;
  }
  else {
    return services_error(t('Project not found'), 404);
  }
}

/**
 * Creates a new Project based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $project
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   An associative array contained the new node's nid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_form_submit()
 */
function erpal_api_unify_project_resource_create($project) {

  $project = erpal_api_unify_project_prepare_data($project);
  try {
    $node = entity_create('node', $project);
    node_save($node);
    return array('id' => $node->nid);
  }
  catch (Exception $exception) {
    return services_error($exception->getMessage(), 406);
  }
}


/**
 * Updates a new project based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $nid
 *   Node ID of the project we're editing.
 * @param $project
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   The node's nid.
 *
 * @see drupal_form_submit()
 */
function erpal_api_unify_project_resource_update($nid, $project) {
  $node_project = node_load($nid);
  $old_node_project = get_object_vars($node_project);
  $project = erpal_api_unify_project_prepare_data($project);
  $project = array_merge($old_node_project, $project);

  try {
    node_save((object) $project);
    return array('id' => $nid);
  }
  catch (Exception $exception) {
    return services_error($exception->getMessage(), 406);
  }
}

/**
 * Prepare data for node resource definition.
 */
function erpal_api_unify_project_prepare_data($project , $type = 'erpal_project') {
  module_load_include('inc', 'services', 'resources/node_resource');
  $node = array();

  // Prepare project properties for currect working with node API.
  $prepare = function (&$node, $project, $type) {

    // If language is passed use it, if not use language none.
    if (!empty($project['language'])) {
      $node['language'] = $project['language'];
    }
    else {
      $node['language'] = LANGUAGE_NONE;
    }

    // Set project name to the node title.
    if (!empty($project['name'])) {
      if (empty($node['title'])) {
        $node['title'] = $project['name'];
      }
    }

    // Set project description to the node body.
    if (!empty($project['description'])) {
      if (empty($node['body'])) {
        $node['body'][$node['language']][0]['value'] = $project['description'];
      }
    }

    // Set project author to the node.
    if (!empty($project['author_id']) && is_numeric($project['author_id'])) {
      if (user_load($project['author_id'])) {
        $node['uid'] = $project['author_id'];
      }
    }

    // Project is "project" node bundle.
    if (!empty($project['type'])) {
      $node['type'] = $project['type'];
    }
    else {
      $node['type'] = $type;
    }

  };

  if (!empty($project[0])) {
    foreach ($project as &$project_instance) {
      if (is_array($project_instance)) {
        $prepare($node, $project_instance, $type);
        $project_instance = $node;
      }
    }
  }
  else {
    $prepare($node, $project, $type);
    $project = $node;
  }
  
  return $project;
}

/**
 * Delete a project given its nid.
 *
 * @param int $nid
 *   Node ID of the project we're deleting.
 * @return bool
 *   Always returns true.
 */
function erpal_api_unify_project_resource_delete($nid) {
  node_delete($nid);
  return TRUE;
}

/**
 * Return an array of optionally paged nids baed on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/node?fields=nid,vid&parameters[nid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only nid and vid defined, where
 * nid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @return
 *   An array of node objects.
 *
 * @todo
 *   Evaluate the functionality here in general. Particularly around
 *     - Do we need fields at all? Should this just return full nodes?
 *     - Is there an easier syntax we can define which can make the urls
 *       for index requests more straightforward?
 */
function erpal_api_unify_project_resource_index($page, $fields, $parameters, $page_size) {
  $parameters['type'] = 'erpal_project';
  return erpal_api_unify_node_resource_index($page, $fields, $parameters, $page_size);
}

/**
 * Return an array of optionally paged nids baed on a set of criteria.
 */
function erpal_api_unify_node_resource_index($page, $fields, $parameters, $page_size) {
  erpal_api_unify_set_pager();
  $node_select = db_select('node', 't')
    ->extend('PagerDefault')
    ->limit($page_size)
    ->addTag('node_access')
    ->orderBy('sticky', 'DESC')
    ->orderBy('created', 'DESC');

  erpal_api_unify_resource_build_index_query($node_select, $page, $fields, $parameters, $page_size, 'node');

  if (!user_access('administer nodes')) {
    $node_select->condition('status', 1);
  }

  $results = services_resource_execute_index_query($node_select);
  $nodes = services_resource_build_index_list($results, 'node', 'nid');
  $projects = erpal_api_unify_list_settings($page_size);

  foreach ($nodes as $node) {
    $project = new stdClass();
    $project->id = $node->nid;
    $project->name = $node->title;
    $projects['items'][] = $project;
  }
  return $projects;
}

/**
 * Determine whether the current user can access a project resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see node_access()
 */
function erpal_api_unify_project_resource_access($op = 'view', $args = array()) {
  foreach ($args as $arg) {

    // Ensure that we have id of Project node.
    if (is_numeric($arg)) {
      $project = node_load($arg);
      if ($project->type != 'erpal_project') {
        return services_error(t('Project not found'), 404);
      }
    }

    // Name for Project node required.
    if (in_array($op, array('create', 'update')) && is_array($arg) && empty($arg['name'])) {
      return services_error(t('Project name is required'), 406);
    }
  }
  return _node_resource_access($op, erpal_api_unify_project_prepare_data($args));
}

/**
 * Generates an array tasks attached to a project.
 *
 * @param $nid
 *   Number. Node ID
 * @return
 *   Array. A list of all tasks from the given node
 */
function erpal_api_unify_project_resource_tasks($nid, $page_size) {

  module_load_include('inc', 'erpal_api_unify', 'resources/project_management/erpal_api_unify.task');

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'erpal_task')
    ->fieldCondition('field_task_project', 'target_id', $nid);
  $result = $query->execute();

  $items = array();
  if (!empty($result['node'])) {
    foreach ($result['node'] as $task) {
      $items[] = $task->nid;
    }
  }
  return erpal_api_unify_task_resource_index(0, '*', array('nid' => implode(',', $items)), $page_size);
}

/**
 * Generates an array time entries attached to a project.
 *
 * @param $nid
 *   Number. Node ID
 * @return
 *   Array. A list of all time entries from the given node
 */
function erpal_api_unify_project_resource_time_entries($nid, $page_size) {
  $query = new EntityFieldQuery();
  module_load_include('inc', 'erpal_api_unify', 'resources/project_management/erpal_api_unify.time_entry');
  // Get all tasks ids from project.
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'erpal_task')
    ->fieldCondition('field_task_project', 'target_id', $nid);
  $result = $query->execute();
  $items = array();
  if (!empty($result['node'])) {
    foreach ($result['node'] as $task) {
      $items[] = $task->nid;
    }
    $items[] = $nid;

    // Get all time entries for project and for tasks which related to this
    // project.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'erpal_output')
      ->entityCondition('bundle', 'time')
      ->fieldCondition('field_output_time_refers', 'target_id', $items, 'IN');
    $result = $query->execute();

    $items = array();
    if (!empty($result['erpal_output'])) {
      foreach ($result['erpal_output'] as $erpal_output) {
        $items[] = $erpal_output->output_id;
      }
    }
  }
  return erpal_api_unify_time_entry_resource_index(0, '*', array('output_id' => implode(',', $items)), $page_size);
}
